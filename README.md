SpringBoot and Hibernate Micro-services: Tending to your Data Relationships
====================

This repo contains the SpringBoot Hibernate micro-service project and MySQL schema dump discussed within the respective PaulsDevBlog.com article. 
https://www.paulsdevblog.com/springboot-and-hibernate-tending-to-your-data-relationships/

For other articles and code, please visit:
https://www.paulsdevblog.com