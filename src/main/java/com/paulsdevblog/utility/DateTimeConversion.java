
package com.paulsdevblog.utility;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Calendar;


/**
 * Class for DateTime conversions
 *
 * @author PaulsDevBlog.com
 */
@Component
public class DateTimeConversion {
    
    /**
     * Return standard ISO 8601 date string from Java.date
     * 
     * @param paramDate java.date
     * @return String ISO8601
     */
    public static String dateToISO8601( java.util.Date paramDate ) {
        
        String string_datetime = null;
        
        try {
            
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
            
            ZonedDateTime zonedDateTime = paramDate.toInstant().atZone( ZoneId.systemDefault() );
            
            string_datetime = zonedDateTime.format( formatter );
            
            return string_datetime;
            
        } catch (Exception e) { 
            return null; 
        }

    }
    
    /**
     * Return standard Unix Epoch second from Java.date
     * 
     * @param paramDate java.date
     * @return long Epoch
     */
    public static long dateToEpochSecond( java.util.Date paramDate ) {
        
        long epoch_second = 0;
        
        try {

            ZoneId zoneId = ZoneId.systemDefault();
            
            epoch_second = paramDate.toInstant().atZone( zoneId ).toEpochSecond();
            
            return epoch_second;
            
        } catch (Exception e) { 
            return 0; 
        }

    }
    
    /**
     * Return Java.date from standard Unix Epoch second
     * 
     * @param dtEpochSecond long
     * @return java.date
     */
    public static Date epochSecondToDate( long dtEpochSecond ) {
        
        try {
         
            java.util.Date resultDT = new Date();
            
            resultDT.setTime((long)dtEpochSecond*1000);
            
            return resultDT;
            
        } catch (Exception e) { 
            return new Date(); 
        }

    }
    
}
