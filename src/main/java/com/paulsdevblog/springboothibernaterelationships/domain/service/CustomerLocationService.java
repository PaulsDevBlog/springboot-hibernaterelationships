
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import com.paulsdevblog.springboothibernaterelationships.domain.model.CustomerLocation;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.CustomerLocationRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerLocationDTO;
import com.paulsdevblog.exceptions.ServiceLayerDataException;

/**
 * Service layer interface for 
 * CustomerLocation
 *
 * @author PaulsDevBlog.com
 */
public interface CustomerLocationService {
    
    public List<CustomerLocation> findAll() throws ServiceLayerDataException;
    
    public List<CustomerLocation> findByCustomerLocationUuid( String customerLocationUuid ) throws ServiceLayerDataException;
    public List<CustomerLocation> findByCustomerLocationCode( String customerLocationCode ) throws ServiceLayerDataException;
    
    public CustomerLocation getOneByCustomerLocationUuid( String customerLocationUuid ) throws ServiceLayerDataException;
    
    public CustomerLocation insertCustomerLocation( CustomerLocationDTO customerLocationDTO ) throws ServiceLayerDataException;
    public CustomerLocation updateCustomerLocation( CustomerLocationDTO customerLocationDTO ) throws ServiceLayerDataException;
    
}
