
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationType;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.LocationTypeRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.service.LocationTypeService;

import com.paulsdevblog.exceptions.ServiceLayerDataException;


/**
 * Service layer implementation for 
 * LocationType
 *
 * @author PaulsDevBlog.com
 */
@Service
public class LocationTypeServiceImpl implements LocationTypeService {
    
    private static final Logger logger = LoggerFactory.getLogger(LocationTypeServiceImpl.class );
    
    @Autowired
    private LocationTypeRepository locationTypeRepository;

    @Autowired
    public LocationTypeServiceImpl( LocationTypeRepository locationTypeRepository ){
        this.locationTypeRepository = locationTypeRepository;
    }
    
    /**
     * Find list of ALL LocationType
     * 
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationType> findAll() throws ServiceLayerDataException {
        try {

            return this.locationTypeRepository.findAll();

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    /**
     * Find list of LocationType by locationTypeUuid
     * 
     * @param locationTypeUuid parameter UUID
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationType> findByLocationTypeUuid( String locationTypeUuid ) throws ServiceLayerDataException {
        try {

            return this.locationTypeRepository.findByLocationTypeUuidAllIgnoreCaseOrderByLocationTypeUuidAsc(String.valueOf( locationTypeUuid ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    /**
     * Find list of LocationType by locationTypeCode
     * 
     * @param locationTypeCode parameter code
     * @return list of LocationType 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationType> findByLocationTypeCode( String locationTypeCode ) throws ServiceLayerDataException {
        try {

            return this.locationTypeRepository.findByLocationTypeCodeAllIgnoreCaseOrderByLocationTypeCodeAsc(String.valueOf( locationTypeCode ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
}
