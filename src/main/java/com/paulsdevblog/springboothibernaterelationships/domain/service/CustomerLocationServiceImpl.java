
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.CustomerLocation;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.CustomerLocationRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.service.CustomerLocationService;

import com.paulsdevblog.exceptions.ServiceLayerDataException;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerLocationDTO;


/**
 * Service layer implementation for 
 * CustomerLocation
 *
 * @author PaulsDevBlog.com
 */
@Service
public class CustomerLocationServiceImpl implements CustomerLocationService {
    
    private static final Logger logger = LoggerFactory.getLogger(CustomerLocationServiceImpl.class );
    
    @Autowired
    private CustomerLocationRepository customerLocationRepository;

    @Autowired
    public CustomerLocationServiceImpl( CustomerLocationRepository customerLocationRepository ){
        this.customerLocationRepository = customerLocationRepository;
    }
    
    /**
     * Find all and list CustomerLocation
     * 
     * @return List of CustomerLocation
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<CustomerLocation> findAll() throws ServiceLayerDataException {
        try {

            return this.customerLocationRepository.findAll();

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    /**
     * Find list of CustomerLocation matching customerLocationUuid
     * 
     * @param customerLocationUuid parameter UUID
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<CustomerLocation> findByCustomerLocationUuid( String customerLocationUuid ) throws ServiceLayerDataException {
        try {

            return this.customerLocationRepository.findByCustomerLocationUuidAllIgnoreCaseOrderByCustomerLocationUuidAsc(String.valueOf( customerLocationUuid ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }

    /**
     * Find list of CustomerLocation matching customerLocationName
     * 
     * @param customerLocationName parameter name
     * @return list of CustomerLocation 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<CustomerLocation> findByCustomerLocationCode( String customerLocationCode ) throws ServiceLayerDataException {
        try {

            return this.customerLocationRepository.findByCustomerLocationCodeAllIgnoreCaseOrderByCustomerLocationCodeAsc(String.valueOf( customerLocationCode ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    
    @Override
    public CustomerLocation getOneByCustomerLocationUuid( String customerLocationUuid ) throws ServiceLayerDataException{
        try {

            List<CustomerLocation> customerLocationList =  this.customerLocationRepository.findByCustomerLocationUuidAllIgnoreCaseOrderByCustomerLocationUuidAsc(String.valueOf( customerLocationUuid ) );
            
            if ( !customerLocationList.isEmpty() ) {
                
                return customerLocationList.get(0);
                
            } else {
                
                return null;
                
            }

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    @Override
    public CustomerLocation insertCustomerLocation( CustomerLocationDTO customerLocationDTO ) throws ServiceLayerDataException{
        try {

            //Setup Entity for Hibernate JPA action
            CustomerLocation customerLocation = new CustomerLocation();
            
            //Null keys to ensure Hibernate JPA treats this and Entity INSERT
            customerLocation.setCustomerLocationRef( null );
            customerLocation.setCustomerLocationUuid( null );

            customerLocation.setCustomerLocationCode( customerLocationDTO.getCustomerLocationCode() );  
            
            customerLocation.setCustomerUuid( customerLocationDTO.getCustomerUuid() );  
            customerLocation.setLocationTypeCode( customerLocationDTO.getLocationTypeCode() );  
            customerLocation.setLocationRegionCode(customerLocationDTO.getLocationRegionCode() );  
            
            customerLocation.setLastModifiedUser( customerLocationDTO.getLastModifiedUser() );  

            //Hibernate JPA save and flush (commit) persist
            CustomerLocation returnCustomerLocation = this.customerLocationRepository.saveAndFlush( customerLocation );

            //Return modified Entity
            return returnCustomerLocation;

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    
    @Override
    public CustomerLocation updateCustomerLocation( CustomerLocationDTO customerLocationDTO ) throws ServiceLayerDataException{
        try {

            //Get keys from DTO
            String customerLocationUuid = String.valueOf( customerLocationDTO.getCustomerLocationUuid());

            //Get current record with Ref and UUID keys or Hibernate JPA will try to set ref key to null, and update will fail
            CustomerLocation customerLocation = this.getOneByCustomerLocationUuid(customerLocationUuid );
            
            if ( customerLocation == null ) { 
                throw new ServiceLayerDataException( "Record not Found. Cannot Update.", String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() );
            } 

            customerLocation.setCustomerLocationCode( customerLocationDTO.getCustomerLocationCode() );  
            
            customerLocation.setCustomerUuid( customerLocationDTO.getCustomerUuid() );  
            customerLocation.setLocationTypeCode( customerLocationDTO.getLocationTypeCode() );  
            customerLocation.setLocationRegionCode(customerLocationDTO.getLocationRegionCode() );  
            
            customerLocation.setLastModifiedUser( customerLocationDTO.getLastModifiedUser() );  

            //Hibernate JPA save and flush (commit) persist
            CustomerLocation returnCustomerLocation = this.customerLocationRepository.saveAndFlush( customerLocation );

            //Return modified Entity
            return returnCustomerLocation;

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
}
