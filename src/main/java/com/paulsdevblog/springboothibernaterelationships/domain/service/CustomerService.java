
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import com.paulsdevblog.springboothibernaterelationships.domain.model.Customer;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.CustomerRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.dto.CustomerDTO;

import com.paulsdevblog.exceptions.ServiceLayerDataException;

/**
 * Service layer interface for 
 * Customer
 *
 * @author PaulsDevBlog.com
 */
public interface CustomerService {
    
    public List<Customer> findAll() throws ServiceLayerDataException;
    
    public List<Customer> findByCustomerUuid( String customerUuid ) throws ServiceLayerDataException;
    public List<Customer> findByCustomerName( String customerName ) throws ServiceLayerDataException;
    
    public Customer getOneByCustomerUuid( String customerUuid ) throws ServiceLayerDataException;
    
    public Customer insertCustomer( CustomerDTO customerDTO ) throws ServiceLayerDataException;
    public Customer updateCustomer( CustomerDTO customerDTO ) throws ServiceLayerDataException;
    
}
