
package com.paulsdevblog.springboothibernaterelationships.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationRegion;
import com.paulsdevblog.springboothibernaterelationships.domain.repository.LocationRegionRepository;
import com.paulsdevblog.springboothibernaterelationships.domain.service.LocationRegionService;

import com.paulsdevblog.exceptions.ServiceLayerDataException;


/**
 * Service layer implementation for 
 * LocationRegion
 *
 * @author PaulsDevBlog.com
 */
@Service
public class LocationRegionServiceImpl implements LocationRegionService {
    
    private static final Logger logger = LoggerFactory.getLogger(LocationRegionServiceImpl.class );
    
    @Autowired
    private LocationRegionRepository locationRegionRepository;

    @Autowired
    public LocationRegionServiceImpl( LocationRegionRepository locationRegionRepository ){
        this.locationRegionRepository = locationRegionRepository;
    }
    
    /**
     * Find all and list LocationRegion
     * 
     * @return List of LocationRegion
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationRegion> findAll() throws ServiceLayerDataException {
        try {

            return this.locationRegionRepository.findAll();

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
    /**
     * Find list of LocationRegion matching locationRegionUuid
     * 
     * @param locationRegionUuid parameter UUID
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationRegion> findByLocationRegionUuid( String locationRegionUuid ) throws ServiceLayerDataException {
        try {

            return this.locationRegionRepository.findByLocationRegionUuidAllIgnoreCaseOrderByLocationRegionUuidAsc(String.valueOf( locationRegionUuid ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }

    /**
     * Find list of LocationRegion matching locationRegionCode
     * 
     * @param locationRegionCode parameter code
     * @return list of LocationRegion 
     * @throws ServiceLayerDataException 
     */
    @Override
    public List<LocationRegion> findByLocationRegionCode( String locationRegionCode ) throws ServiceLayerDataException {
        try {

            return this.locationRegionRepository.findByLocationRegionCodeAllIgnoreCaseOrderByLocationRegionCodeAsc(String.valueOf( locationRegionCode ) );

        } catch (Exception ex){
            
            String currentMethod =  String.valueOf( new Object(){}.getClass().getEnclosingMethod().getName() ).trim() ;
            String currentExceptionMsg = String.valueOf( ex.getMessage() );
            
            logger.error( currentMethod + " ExceptionMsg: " + currentExceptionMsg );
            
            throw new ServiceLayerDataException( currentExceptionMsg, currentMethod );
            
        }
    }
    
}
