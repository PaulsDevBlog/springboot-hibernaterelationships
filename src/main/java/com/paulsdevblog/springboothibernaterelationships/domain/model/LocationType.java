
package com.paulsdevblog.springboothibernaterelationships.domain.model;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import org.springframework.core.annotation.*;
import org.springframework.beans.factory.annotation.*;

import com.paulsdevblog.springboothibernaterelationships.domain.views.EntityViews;
import com.paulsdevblog.utility.DateTimeConversion;

/**
 * Simple Spring-JPA-Hibernate entity example that represents table 
 * location_type
 *
 * @author PaulsDevBlog.com
 */
@Entity
@Table(name = "location_type", uniqueConstraints = {
    @UniqueConstraint( columnNames = {"location_type_uuid"} )
})
@XmlRootElement
@JsonPropertyOrder(
    { 
        "locationTypeUuid",
        "locationTypeCode", 

        "locationTypeName",
        
        "lastModifiedAction", 
        "lastModifiedUser", 
           
        "LastModifiedDt", 
        "LastModifiedDtAsIso8601", 
        "LastModifiedDtAsEpochSecond"
    }
)
public class LocationType {
    
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "location_type_uuid", nullable = false, length = 50)
    private String locationTypeUuid;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "location_type_code", nullable = false, length = 25)
    private String locationTypeCode;
    
    //This is the pointer back to the ManyToOne relationship defined in entity CustomerLocation
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "locationType", fetch = FetchType.LAZY )
    private List<CustomerLocation> customerLocationList;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "location_type_name", nullable = false, length = 50)
    private String locationTypeName;
    
    @Size(max = 255)
    @Column(name = "last_modified_user", length = 255)
    private String lastModifiedUser;
    
    @Column(name = "last_modified_dt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDt;
    
    @Size(max = 25)
    @Column(name = "last_modified_action", length = 25)
    private String lastModifiedAction;
    
    public LocationType(){}

    public LocationType(
        String locationTypeUuid, 
        String locationTypeCode, 
        String locationTypeName, 
        String lastModifiedUser, 
        Date lastModifiedDt, 
        String lastModifiedAction
    ) {
        this.locationTypeUuid = locationTypeUuid;
        this.locationTypeCode = locationTypeCode;
        this.locationTypeName = locationTypeName;
        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDt = lastModifiedDt;
        this.lastModifiedAction = lastModifiedAction;
    }

    @JsonView( { EntityViews.Details.class } )
    public String getLocationTypeUuid() {
        return locationTypeUuid;
    }

    public void setLocationTypeUuid(String locationTypeUuid) {
        this.locationTypeUuid = locationTypeUuid;
    }

    @JsonView( { EntityViews.List.class } )
    public String getLocationTypeCode() {
        return locationTypeCode;
    }

    public void setLocationTypeCode(String locationTypeCode) {
        this.locationTypeCode = locationTypeCode;
    }
    
    @JsonView( { EntityViews.EntityChildren.class } )
    public List<CustomerLocation> getCustomerLocationList() {
        return customerLocationList;
    }

    public void setCustomerLocationList(List<CustomerLocation> customerLocationList) {
        this.customerLocationList = customerLocationList;
    }

    @JsonView( { EntityViews.List.class } )
    public String getLocationTypeName() {
        return locationTypeName;
    }

    public void setLocationTypeName(String locationTypeName) {
        this.locationTypeName = locationTypeName;
    }


    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @JsonView( { EntityViews.Details.class } )
    public Date getLastModifiedDt() {
        return lastModifiedDt;
    }
    
    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedDtAsIso8601() {
        return DateTimeConversion.dateToISO8601( lastModifiedDt );
    }
    
    @JsonView( { EntityViews.Details.class } )
    public long getLastModifiedDtAsEpochSecond() {
        return DateTimeConversion.dateToEpochSecond( lastModifiedDt );
    }

    public void setLastModifiedDt(Date lastModifiedDt) {
        this.lastModifiedDt = lastModifiedDt;
    }

    @JsonView( { EntityViews.Details.class } )
    public String getLastModifiedAction() {
        return lastModifiedAction;
    }

    public void setLastModifiedAction(String lastModifiedAction) {
        this.lastModifiedAction = lastModifiedAction;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.locationTypeUuid);
        hash = 29 * hash + Objects.hashCode(this.locationTypeCode);
        hash = 29 * hash + Objects.hashCode(this.locationTypeName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocationType other = (LocationType) obj;
        if (!Objects.equals(this.locationTypeCode, other.locationTypeCode)) {
            return false;
        }
        if (!Objects.equals(this.locationTypeName, other.locationTypeName)) {
            return false;
        }
        return true;
    }

    
    

    @Override
    public String toString() {
        return "LocationType{" 
                
                    + "locationTypeUuid=" + String.valueOf( this.locationTypeUuid ) 
                    + ", locationTypeCode=" + String.valueOf( this.locationTypeCode ) 
                
                    + ", locationTypeName=" + String.valueOf( this.locationTypeName )  
                
                    + ", lastModifiedUser=" + String.valueOf( this.lastModifiedUser )  
                    + ", lastModifiedDtAsIso8601=" + this.getLastModifiedDtAsIso8601() 
                    + ", lastModifiedDtAsEpochSecond=" + String.valueOf( this.getLastModifiedDtAsEpochSecond() )
                    + ", lastModifiedAction=" + String.valueOf( this.lastModifiedAction ) 
                + '}';
    }

    
    
    
}
