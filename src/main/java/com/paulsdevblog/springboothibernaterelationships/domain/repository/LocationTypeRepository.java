
package com.paulsdevblog.springboothibernaterelationships.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.paulsdevblog.springboothibernaterelationships.domain.model.LocationType;


/**
 * Spring Hibernate JPA repository for 
 * LocationType
 * 
 * Uses named queries from model and spring-JPA method-name query definition language
 * 
 * Refer to https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
 * Refer to https://projects.spring.io/spring-data-jpa/ 
 * 
 * @author PaulsDevBlog.com
 */
public interface LocationTypeRepository extends JpaRepository<LocationType, Long> {
    
    public List<LocationType> findAll();
    
    public List<LocationType> findByLocationTypeUuidAllIgnoreCaseOrderByLocationTypeUuidAsc( @Param("locationTypeUuid") String locationTypeUuid );
    public List<LocationType> findByLocationTypeCodeAllIgnoreCaseOrderByLocationTypeCodeAsc( @Param("locationTypeCode") String locationTypeCode );
    
}
