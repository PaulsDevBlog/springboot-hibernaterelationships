package com.paulsdevblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main entry point for application
 * 
 * @author PaulsDevBlog.com
 */
@SpringBootApplication
public class SpringBootHibernateRelationshipsApplication {
    
    public static final Logger logger = LoggerFactory.getLogger( SpringBootHibernateRelationshipsApplication.class );

    public static void main(String[] args) {
        
        SpringApplication.run(SpringBootHibernateRelationshipsApplication.class, args);
        
        logger.info("--Application Started--");
        logger.info("SpringBoot and Hibernate Entity Relationships Micro-Service!");
    }
}
